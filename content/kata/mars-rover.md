---
title: "Mars Rover"
draft: false
date: "2022-04-21"
---



You’re part of the team that make Mars Rover. Develop the simulator programm that take commands and map, they translates the commands and show the result position and direction of the Rover.


## Requirements

Your program take in input 
* the starting point (x,y) of a rover and the direction (N,S,E,W) it is facing
* a map describing obstacles positions
* the list of commands that move and turn the rover (⬆️ : move forward, ➡️ : turn right 90°, ⬅️ : turn left 90°)

When the rover encounters an obstacle it do nothing.

The map could be describe by a string like :

```
🟩🟩🌳🟩🟩
🟩🟩🟩🟩🟩
🟩🟩🟩🌳🟩
🟩🌳🟩🟩🟩
➡️🟩🟩🟩🟩
```
or 

`̀̀ `
🟫🟫🪨🟫🟫
🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫
⬆️🟫🟫🟫🟫
`̀̀ `
